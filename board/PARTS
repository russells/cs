# -*- org -*-

# Create a PDF document with C-c C-e p
# View a PDF document with C-c C-e d

#+LATEX_CLASS: article
#+OPTIONS: toc:nil
#+TITLE: CS Parts
#+AUTHOR:

| Part                  | Package  | Desc                       | Qty |
|-----------------------+----------+----------------------------+-----|
| Board                 |          | PCB, "CS v2"               |   1 |
| ATmega1284P           | LQFP100  | MCU                        |   1 |
| AT7303BRZ             | SOIC8    | DAC                        |   1 |
| MAX9700AEUB+          | MSPO10A  | Audio amplifier            |   1 |
| Capacitor, 1uF        | SMD 0805 | Audio coupling             |   2 |
| Capacitor, 470pF      | SMD 0805 | Audio filter               |   2 |
| Capacitor, 0.1uF      | SMD 0805 | Power supply bypass        |   9 |
| Resistor, 10k         | SMD 0805 | Audio filter               |   2 |
| Resistor, 33k         | SMD 0805 | Audio filter               |   2 |
| Resistor, 100k        | SMD 0805 | Pullups                    |   3 |
| Resistor, 330         | SMD 0805 | LED current limit          |   1 |
| LED, red              | SMD 0805 | Debugging                  |   1 |
| Connector, 3x2, 0.1in |          | ICSP                       |   1 |
| Connector, 6x1, 0.1in |          | Serial                     |   1 |
| Connector, 2x1, 0.1in |          | Power, speaker, lid switch |   3 |
| Box                   |          | Altronics H8976            |   1 |
| Speaker 1W            |          | Altronics C0604B           |   1 |
| Switch                | ?        | Lid switch                 |   1 |

Notes:

1. PINA and PINB are solder blob jumpers, not resistors, but they have SMD
   0805 footprints.

1. SPOUT1 and SPOUT2 have footprints for 2x1 0.1 inch jumpers, but are
   shorted across underneath the PCB.  They only exist for possible inline
   components to the speaker, and are not expected to be populated.

1. The LED is not essential, but is handy for debugging.
