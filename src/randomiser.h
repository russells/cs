#ifndef randomiser_h_INCLUDED
#define randomiser_h_INCLUDED

#include "qpn_port.h"

struct Randomiser {
	QActive super;
	uint8_t counter;
};

extern struct Randomiser randomiser;

void randomiser_ctor(void);

#endif // randomiser_h_INCLUDED
