#include <stdint.h>
#include "wdt.h"
#include "runner.h"
#include "lidtimer.h"
#include "randomiser.h"
#include "sounder.h"
#include "qpn_port.h"
#include "serial.h"
#include "toggle-pin.h"
#include "morse.h"
#include "config.h"

#include "sound.h"
#include "sounds.h"


Q_DEFINE_THIS_MODULE(cs);


static QEvent runnerQueue[4];
static QEvent lidtimerQueue[4];
static QEvent randomiserQueue[4];


QActiveCB const Q_ROM Q_ROM_VAR QF_active[] = {
        { (QActive *)0              , (QEvent *)0      , 0                        },
        { (QActive *)(&runner)      , runnerQueue      , Q_DIM(runnerQueue)       },
        { (QActive *)(&lidtimer)    , lidtimerQueue    , Q_DIM(lidtimerQueue)     },
        { (QActive *)(&randomiser)  , randomiserQueue  , Q_DIM(randomiserQueue)   },
};


Q_ASSERT_COMPILE(QF_MAX_ACTIVE == Q_DIM(QF_active) - 1);


int main(void)
{
	readconfigs();

	TOGGLE_BEGIN();
	TOGGLE_ON(); _delay_ms(50); TOGGLE_OFF();
	_delay_ms(50);
	TOGGLE_ON(); _delay_ms(50); TOGGLE_OFF();

	sounder_init();
	serial_init();

	sei();

	SERIALSTR("\r\n** CS **\r\n");SERIAL_DRAIN();
	SERIALSTR(V "\r\n");          SERIAL_DRAIN();
	SERIALSTR(D "\r\n");          SERIAL_DRAIN();
	SERIALSTR(SOUNDS "\r\n");     SERIAL_DRAIN();
	SERIALSTR("\r\n");            SERIAL_DRAIN();

	{
		struct SoundInfo si;
		for (uint8_t i=0; i< NSOUNDS; i++) {
			get_1_sound(i, &si);
			SERIALSTR("    Sound ");
			SERIAL_SEND_INT(i);
			SERIALSTR(": @0x");
			SERIAL_SEND_HEX_LONG_INT(si.bytes);
			SERIALSTR("; length ");
			SERIAL_SEND_INT(si.length);
			SERIALSTR(" (0x");
			SERIAL_SEND_HEX_INT(si.length);
			SERIALSTR(")\r\n");
			SERIAL_DRAIN();
		}
		SERIALSTR("\r\n");
	}

	wdt_reset();
	_delay_ms(500);
	wdt_reset();

	if (configA) {
		SERIALSTR("configA ON\r\n");
	}
	if (configB) {
		SERIALSTR("configB ON\r\n");
	}

	runner_ctor();
	lidtimer_ctor();
	randomiser_ctor();

	QF_run();
}


void QF_onStartup(void)
{
	watchdog_start();
}


void Q_onAssert(char const Q_ROM * const Q_ROM_VAR file, int line)
{
	watchdog_stop();
	SERIAL_ASSERT_NOSTOP(file, line);
	morse_assert(file, line);
}


void QF_onIdle(void)
{
	TOGGLE_OFF();

	if (serial()) {
		/* Turn off everything except the USART. */
		PRR0 = ~(1 << PRUSART0);
		/* Idle sleep mode. */
		SMCR =  (0 << SM2) |
			(0 << SM1) |
			(0 << SM0) |
			(1 << SE);
	} else {
		/* Turn off everything. */
		PRR0 = 0xff;
		/* Power down sleep mode. */
		SMCR =  (0 << SM2) |
			(1 << SM1) |
			(0 << SM0) |
			(1 << SE);
	}
	/* Don't separate the following two assembly instructions.  See Atmel's
	   NOTE03. */
	__asm__ __volatile__ ("sei" "\n\t" :: );
	__asm__ __volatile__ ("sleep" "\n\t" :: );

	CB(SMCR, SE);
}
