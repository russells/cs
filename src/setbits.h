#ifndef setbits_h_INCLUDED
#define setbits_h_INCLUDED

#define SETBIT(p,b)   do { (p) |=    (1 << (b)) ; } while (0)
#define CLEARBIT(p,b) do { (p) &= (~ (1 << (b))); } while (0)
#define XORBIT(p,b)   do { (p) ^=    (1 << (b)) ; } while (0)

#define SB(p,b) SETBIT(p,b)
#define CB(p,b) CLEARBIT(p,b)
#define XB(p,b) XORBIT(p,b)

#define BITISSET(p,b)   ( (p) & (1 << (b)) )
#define BITISCLEAR(p,b) (! BITISSET(p,b) )

#define BS(p,b) BITISSET(p,b)
#define BC(p,b) BITISCLEAR(p,b)

#endif // setbits_h_INCLUDED
