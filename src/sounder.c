#include "qpn_port.h"
#include <avr/io.h>
#include <avr/cpufunc.h>
#include <stdint.h>
#include "sounder.h"
#include "pins.h"
#include "ad7303.h"
#include "sound.h"
#include "setbits.h"
#include "truefalse.h"
#include "serial.h"
#include "config.h"


Q_DEFINE_THIS_MODULE(sounder);


#define sync(onoff)					\
	do {						\
		if (onoff) {				\
			CB(DACSEL_PORT, DACSEL_PIN);	\
		} else {				\
			SB(DACSEL_PORT, DACSEL_PIN);	\
		}					\
	} while (0)


#define AMP_ON()				\
	do {					\
		SB(AMPSHDN_PORT, AMPSHDN_PIN);	\
	} while (0)

#define AMP_OFF()				\
	do {					\
		CB(AMPSHDN_PORT, AMPSHDN_PIN);	\
	} while (0)


#define spi_wait()							\
	do {								\
		/* Nothing, wait until the bit is set. */		\
	} while ( BITISCLEAR(SPSR,SPIF) );


#define spi_byte(x)							\
	do {								\
		spi_wait();						\
		SPDR = x;						\
		spi_wait();						\
	} while (0)


/** Put the DAC into low power mode. */
static void shutdown_dac(void);


/**
 * Initialise the SPI hardware.
 *
 * We assume that the CLKio is 8 MHz (calibrated RC oscillator with the CKDIV8
 * fuse not programmed.)  We want to push sound samples out at 8kHz, so we have
 * 1000 cycles per sample, and there are four bytes per sample.  That gives 250
 * cycles per byte, or 31.25 cycles per bit, so as a first approximation we set
 * the SPI clock to CLKio/32, and hope that we can keep stuffing bytes in to
 * keep up.  We do this with interrupts off, so the timing should at least be
 * constant and hopefully glitch free.
 *
 * The AD7303 clocks data in on the rising edge, so SPI mode 0 (idle low,
 * leading edge) is appropriate.
 */
static void spi_init(void)
{
	CB( PRR0, PRSPI );	/* Enable the SPI */
	SB( DACSEL_DDR, DACSEL_PIN ); /* /SYNC */
	sync(false);
	SB( DDRB, 4 );		/* SS - ensure this stays output */
	SB( DDRB, 5 );		/* MOSI */
	SB( DDRB, 7 );		/* SCK */
	SPCR =  (1 << SPE ) |
		(0 << DORD) |
		(1 << MSTR) |
		(0 << CPOL) |	/* Idle low, leading (rising) edge */
		(0 << CPHA) |
		(1 << SPR1) |	/* CLKio/32 */
		(0 << SPR0);
	SB( SPSR, SPI2X );	/* CLKio/32 */

	/* The very first byte sent does not enable the SPIF flag.  So we send
	   one byte "blind" (with no receivers enabled) just to do that. */
	SPDR = 0xa5;
	spi_wait();
}


void sounder_init(void)
{
	SB(AMPSHDN_DDR, AMPSHDN_PIN); /* Output */
	CB(AMPSHDN_PORT, AMPSHDN_PIN); /* No pullup */
	AMP_OFF();
	spi_init();
	shutdown_dac();
}


void sounder_send(struct SoundInfo *si)
{
	uint8_t sreg;
	uint16_t index;
	uint8_t control;
	uint8_t data;

	sreg = SREG;
	cli();			/* Do the whole thing with interrupts off. */

	spi_init();		/* If the SPI has been off, it must be
				   reinitialised. */

	/* Ensure that the SPIF flag is clear before we start, by reading the
	   SPSR and accessing SPDR.  We haven't set the DAC's SYNC pin yet, so
	   the write to SPDR is harmless. */
	(void)SPSR;
	SPDR = 0;
	/* Wait for the byte to disappear. */
	spi_wait();

	AMP_ON();


	index = 0;
	for (index=0; index < si->length; index++) {

		data = pgm_read_byte_far(si->bytes + index);

		sync(true);
		control =
			/* Only update the DAC A input register. */
			(0 << AD7303_INTEXT) |
			(0 << AD7303_X     ) |

			(0 << AD7303_PDB   ) |
			(0 << AD7303_PDA   ) |

			(0 << AD7303_LDAC  ) |
			(0 << AD7303_AB    ) |
			(0 << AD7303_CR1   ) |
			(1 << AD7303_CR0   );
		spi_byte(control);
		spi_byte(data);
		sync(false);

		_NOP();
		sync(true);
		control =
			/* Update the DAC B input register, and update both DAC
			   registers. */
			(0 << AD7303_INTEXT) |
			(0 << AD7303_X     ) |

			(0 << AD7303_PDB   ) |
			(0 << AD7303_PDA   ) |

			(1 << AD7303_LDAC  ) |
			(1 << AD7303_AB    ) |
			(0 << AD7303_CR1   ) |
			(1 << AD7303_CR0   );
		spi_byte(control);
		spi_byte(0xff - data);
		sync(false);
	}

	shutdown_dac();
	AMP_OFF();

	SREG = sreg;		/* Interrupts back to where they were. */
}


static void shutdown_dac(void)
{
	uint8_t control;

	/* Power down the DAC channels. */
	sync(true);
	control =
		(0 << AD7303_INTEXT) |
		(0 << AD7303_X     ) |
		(0 << AD7303_LDAC  ) |
		(1 << AD7303_PDB   ) | /* Shut down A. */
		(1 << AD7303_PDA   ) | /* Shut down B. */
		(0 << AD7303_AB    ) |
		(0 << AD7303_CR1   ) | /* Both DAC registers from shift reg */
		(0 << AD7303_CR0   );
	spi_byte(control);
	spi_byte(0x80);		/* Mid level for quiet time. */
	sync(false);
}
