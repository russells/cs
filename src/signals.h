#ifndef signals_h_INCLUDED
#define signals_h_INCLUDED

enum CSSignals {
	WATCHDOG_SIGNAL = Q_USER_SIG,
	SOUND_FINISHED_SIGNAL,
	LID_ON_SIGNAL,
	LID_OFF_SIGNAL,
	LID_ON_LONG_SIGNAL,
};

#endif // signals_h_INCLUDED
