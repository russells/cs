#ifndef sound_h_INCLUDED
#define sound_h_INCLUDED

#include <avr/pgmspace.h>

struct SoundInfo {
	uint_farptr_t bytes;
	uint16_t length;
	uint8_t num;
};

/**
 * Get a random sound.
 */
void get_sound(struct SoundInfo *soundinfo);

#endif // sound_h_INCLUDED
