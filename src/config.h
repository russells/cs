#ifndef config_h_INCLUDED
#define config_h_INCLUDED

#include <stdint.h>

extern uint8_t configA;
extern uint8_t configB;

void readconfigs(void);

#endif // config_h_INCLUDED
