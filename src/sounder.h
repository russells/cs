#ifndef sounder_h_INCLUDED
#define sounder_h_INCLUDED

#include "sound.h"

/** Initialise the amplifier and the SPI. */
void sounder_init(void);

/** Send our sound data out to the DAC, amplifier, and speaker. */
void sounder_send(struct SoundInfo *si);

#endif // sounder_h_INCLUDED
