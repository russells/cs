#ifndef serial_h_INCLUDED
#define serial_h_INCLUDED

#include "qpn_port.h"

extern uint8_t serialOn;

inline uint8_t serial(void) { return serialOn; }


void serial_init(void);
int  serial_send(const char *s);
int  serial_send_rom(char const Q_ROM * const Q_ROM_VAR s);
int  serial_send_int(unsigned int n);
int serial_send_long_int(unsigned long int n);
int  serial_send_hex_int(unsigned int x);
int  serial_send_hex_long_int(unsigned long int x);
int  serial_send_char(char c);
void serial_assert(char const Q_ROM * const Q_ROM_VAR file, int line);
void serial_assert_nostop(char const Q_ROM * const Q_ROM_VAR file, int line);
void serial_drain(void);

/* These macros check that the serial port is on before doing anything.  They
   take three machine instructions when serial is not enabled. */

#define SERIAL_SEND(s)				\
	do {					\
		if (serial()) {			\
			serial_send(s);		\
		}				\
	} while (0)

#define SERIAL_SEND_ROM(s)			\
	do {					\
		if (serial()) {			\
			serial_send_rom(s);	\
		}				\
	} while (0)

#define SERIAL_SEND_INT(n)			\
	do {					\
		if (serial()) {			\
			serial_send_int(n);	\
		}				\
	} while (0)

#define SERIAL_SEND_LONG_INT(n)				\
	do {						\
		if (serial()) {				\
			serial_send_long_int(n);	\
		}					\
	} while (0)

#define SERIAL_SEND_HEX_INT(x)			\
	do {					\
		if (serial()) {		\
			serial_send_hex_int(x); \
		}				\
	} while (0)

#define SERIAL_SEND_HEX_LONG_INT(x)			\
	do {						\
		if (serial()) {				\
			serial_send_hex_long_int(x);	\
		}					\
	} while (0)

#define SERIAL_SEND_CHAR(c)			\
	do {					\
		if (serial()) {			\
			serial_send_char(c);	\
		}				\
	} while (0)

#define SERIAL_ASSERT(file, line)			\
	do {						\
		if (serial()) {				\
			serial_assert(file, line);	\
		}					\
	} while (0)

#define SERIAL_ASSERT_NOSTOP(file, line)			\
	do {							\
		if (serial()) {					\
			serial_assert_nostop(file, line);	\
		}						\
	} while (0)

#define SERIAL_DRAIN()				\
	do {					\
		if (serial()) {			\
			serial_drain();		\
		}				\
	} while (0)
/**
 * Send a constant string (stored in ROM).
 *
 * This macro takes care of the housekeeping required to send a ROM string.  It
 * creates a scope, stores the string in ROM, accessible only inside that
 * scope, and calls serial_send_rom() to output the string.
 */
#define SERIALSTR(s)						\
	do {							\
		if (serial()) {					\
			static const char PROGMEM ss[] = s;	\
			serial_send_rom(ss);			\
		}						\
	} while (0)

#endif
