#include "sound.h"
#include "sounds.h"
#include "qpn_port.h"
#include <stdlib.h>		/* random() */

Q_DEFINE_THIS_MODULE(s);


/**
 * We need at least three sounds so we have a first sound (for when the lid
 * opens), a last sound (for when the lid has been been on for a long time) and
 * one or more other sounds (to play occasionally while the lid is off.)
 */
Q_ASSERT_COMPILE( NSOUNDS >= 3 );


void get_sound(struct SoundInfo *soundinfo)
{
	uint8_t index;

	index = (random() & 0xff) % NSOUNDS;
	get_1_sound(index, soundinfo);
}


/**
 * Later, the first sound will be for when the lid first opens, and the last
 * sound will be the one that is used randomly when the lid has been on for a
 * few days.
 *
 * @todo Function that returns a random sound, but not the first or last.
 *
 * @todo Function that returns the last sound.
 */
