#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include "wdt.h"
#include "runner.h"
#include "signals.h"
#include "serial.h"
#include "toggle-pin.h"


void watchdog_start(void)
{
	watchdog_short();
}


void watchdog_tick(void)
{
	//SERIALSTR(".");
	wdt_reset();
	WDTCSR |= (1 << WDIE);
}


void watchdog_long(void)
{
	wdt_reset();
	wdt_enable(WDTO_8S);
	WDTCSR |= (1 << WDIE);
}


void watchdog_short(void)
{
	wdt_reset();
	wdt_enable(WDTO_250MS);
	WDTCSR |= (1 << WDIE);
}


void watchdog_stop(void)
{
	wdt_disable();
}


/** Save the reset value of the MCUSR. */
uint8_t mcusr_mirror __attribute__ ((section (".noinit")));


/** Ensure this function is in an early text section. */
void get_mcusr(void)
	__attribute__((naked))
	__attribute__((section(".init3")));


/**
 * Save the MCUSR value, reset the watchdog, and set the watchdog timeout high.
 */
void get_mcusr(void)
{
	mcusr_mirror = MCUSR;
	MCUSR = 0;
	wdt_disable();
	wdt_enable(WDTO_8S);
}


/**
 * Do QP-nano timing, and send a signal so the mainline can reset the watchdog.
 *
 * Watchdog resetting is done in the mainline so we can be sure that the event
 * dispatch and event handling systems are running.
 */
SIGNAL(WDT_vect)
{
	TOGGLE_ON();
	QF_tick();
	QActive_postISR(&(runner.super), WATCHDOG_SIGNAL);
}
