#include "runner.h"
#include "wdt.h"
#include "lid.h"
#include "sounder.h"
#include "sound.h"
#include "sounds.h"
#include "signals.h"
#include "serial.h"
#include "stdlib.h"
#include "config.h"
#include "lidtimer.h"
#include "truefalse.h"


Q_DEFINE_THIS_MODULE(runner);

struct Runner runner;


static QState initialState(struct Runner *me);
static QState topState(struct Runner *me);
static QState lidOnState(struct Runner *me);
static QState lidOffState(struct Runner *me);
static QState soundPlayingState(struct Runner *me);
static QState firstSoundState(struct Runner *me);
static QState randomSoundsState(struct Runner *me);
static QState playRandomSoundState(struct Runner *me);

static void wait_for_sound(struct Runner *me, uint8_t soundnum);
static void wait_for_random_sound(struct Runner *me);

uint8_t get_sound_num(void);

static void set_initial_pause_time(struct Runner *me);
static void inc_pause_time(struct Runner *me);


void runner_ctor(void)
{
	QActive_ctor(&(runner.super), (QStateHandler)&initialState);
	set_initial_pause_time(&runner);
	runner.lastSound = 0;
	SERIAL_SEND("    Pause time     = ");
	SERIAL_SEND_LONG_INT(runner.pauseTime);
	SERIAL_SEND("\r\n    Max pause time = ");
	SERIAL_SEND_LONG_INT(runner.maxPauseTime);
	SERIAL_SEND("\r\n");
}


/**
 * Set the initial and maximum pause times.
 *
 * When running normally, the maximum pause time is 4096 times the initial
 * pause time.  This is not actually a maximum, but the value that, when we go
 * over it, we stop increasing.  So for the default 10 seconds initial pause
 * time (set with $(INITIAL_PAUSE_TIME) in Makefile), the maximum pause time is
 * about 13 hours and 48 minutes.
 *
 * In test mode (with configB on), the initial pause time is one second, and
 * the maximum pause time is about 50.5 minutes.
 *
 * When doing the time calculations, ensure they're done with ticks (four per
 * second), not seconds, as this makes a difference when we increment the pause
 * time.
 *
 * @see inc_pause_time()
 */
static void set_initial_pause_time(struct Runner *me)
{
	if (configB) {
		me->pauseTime = 4; /* One second */
		me->maxPauseTime = (4<<11);
	} else {
		me->pauseTime = 4 * INITIAL_PAUSE_TIME;
		/* Cast to uint32_t is required so cpp doesn't do all its
		   arithmetic in 16 bits and cause overflow here.  That can
		   result in maxPauseTime being zero or a different weird
		   value. */
		me->maxPauseTime = ((4*(uint32_t)INITIAL_PAUSE_TIME)<<12);
	}
}


static void inc_pause_time(struct Runner *me)
{
	if (me->pauseTime < me->maxPauseTime) {
		me->pauseTime += (me->pauseTime >> 1);
	}
}


static QState initialState(struct Runner *me)
{
	return Q_TRAN(lidOnState);
}


static QState topState(struct Runner *me)
{
	switch (Q_SIG(me)) {
	case WATCHDOG_SIGNAL:
		watchdog_tick();
		return Q_HANDLED();
	}
	return Q_SUPER(&QHsm_top);
}


static QState lidOnState(struct Runner *me)
{
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		SERIALSTR("Snoozing...\r\n");
		/* Reset the pause time so we start again from scratch the next
		   time the lid is off. */
		set_initial_pause_time(me);
		QActive_post(&(lidtimer.super), LID_ON_SIGNAL);
		return Q_HANDLED();
	case WATCHDOG_SIGNAL:
		watchdog_tick();
		if (lidisoff()) {
			return Q_TRAN(firstSoundState);
		} else {
			return Q_HANDLED();
		}
	}
	return Q_SUPER(topState);
}


static QState lidOffState(struct Runner *me)
{
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		/* Ensure the pause time is correct just before we start
		   playing sounds. */
		set_initial_pause_time(me);
		QActive_post(&(lidtimer.super), LID_OFF_SIGNAL);
		return Q_HANDLED();
	case WATCHDOG_SIGNAL:
		watchdog_tick();
		if (! lidisoff()) {
			return Q_TRAN(lidOnState);
		} else {
			return Q_HANDLED();
		}
	}
	return Q_SUPER(topState);
}


/**
 * Parent state for all states that play sounds.
 *
 * This only handles the watchdog signal, so we can handle the ticks but not
 * transition out if the lid goes back on.
 *
 * Actually, the watchdog signal is not generated while sounds are playing, so
 * this is overkill.
 */
static QState soundPlayingState(struct Runner *me)
{
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		/* We're about to play a sound, so make the watchdog very
		   long. */
		watchdog_long();
		return Q_HANDLED();
	case WATCHDOG_SIGNAL:
		watchdog_tick();
		return Q_HANDLED();
	case SOUND_FINISHED_SIGNAL:
		return Q_TRAN(randomSoundsState);
	case Q_EXIT_SIG:
		/* We're finished playing a sound, so make the watchdog
		   short. */
		watchdog_short();
		QActive_disarm(&(me->super));
		return Q_HANDLED();
	case Q_TIMEOUT_SIG:
		/* We waited, but the sound didn't finish. */
		Q_ASSERT(0);
		return Q_TRAN(topState);
	}
	return Q_SUPER(lidOffState);
}


static QState firstSoundState(struct Runner *me)
{
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		wait_for_sound(me, get_sound_num());
		return Q_HANDLED();
	}
	return Q_SUPER(soundPlayingState);
}


static QState randomSoundsState(struct Runner *me)
{
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		SERIALSTR("Pausing for ");
		SERIAL_SEND_LONG_INT(me->pauseTime);
		SERIALSTR(" ticks\r\n");
		QActive_arm(&(me->super), me->pauseTime);
		return Q_HANDLED();
	case Q_TIMEOUT_SIG:
		return Q_TRAN(playRandomSoundState);
	}
	return Q_SUPER(lidOffState);
}


static QState playRandomSoundState(struct Runner *me)
{
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		wait_for_random_sound(me);
		return Q_HANDLED();
	case Q_EXIT_SIG:
		/* This is the only place where we can increment the pause time
		   after a random sound has been played, but not before any
		   random sound has been played. */
		inc_pause_time(me);
		return Q_HANDLED();
	}
	return Q_SUPER(soundPlayingState);
}


static void wait_for_random_sound(struct Runner *me)
{
	wait_for_sound(me, get_sound_num());
}


static void wait_for_sound(struct Runner *me, uint8_t soundnum)
{
	struct SoundInfo si;

	get_1_sound(soundnum, &si);
	if (serial()) {
		SERIALSTR("\r\nSending sound ");
		SERIAL_SEND_INT(si.num);
		SERIALSTR(": 0x");
		SERIAL_SEND_HEX_LONG_INT(si.bytes);
		SERIALSTR("; length ");
		SERIAL_SEND_INT(si.length);
		SERIALSTR("\r\n");
		SERIAL_DRAIN();
	}
	QActive_arm(&(me->super), 2);
	soundnum ++;
	if (soundnum >= NSOUNDS) {
		soundnum = 0;
	}
	sounder_send(&si);
	QActive_post(&(me->super), SOUND_FINISHED_SIGNAL);
}


/**
 * Get a random sound number.
 */
uint8_t get_sound_num(void)
{
	return (random() & 0xff) % NSOUNDS;
}


/** Ensure we have enough sounds. */
Q_ASSERT_COMPILE( NSOUNDS >= 4 );
