#ifndef lid_h_INCLUDED
#define lid_h_INCLUDED

#include <stdint.h>

void lid_init(void);
uint8_t lidisoff(void);

#endif // lid_h_INCLUDED
