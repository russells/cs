#include "lid.h"
#include "setbits.h"
#include "truefalse.h"
#include "serial.h"
#include <avr/io.h>
#include <avr/cpufunc.h>	/* _NOP() */


#define LIDV_PORT PORTD
#define LIDV_PINS PIND
#define LIDV_DDR DDRD
#define LIDV_PIN 4

#define LID_PORT PORTD
#define LID_PINS PIND
#define LID_DDR DDRD
#define LID_PIN 3


void lid_init(void)
{

	// Lid supply voltage.
	SB( LIDV_DDR, LIDV_PIN );  /* output */
	CB( LIDV_PORT, LIDV_PIN ); /* low */
	// Lid input line.
	CB( LID_DDR, LID_PIN );	 /* input */
	CB( LID_PORT, LID_PIN ); /* no pullup */
}


uint8_t lidisoff(void)
{
	uint8_t lid;

	SB( LIDV_DDR, LIDV_PIN );  /* output */
	SB( LIDV_PORT, LIDV_PIN ); /* high */
	CB( LID_DDR, LID_PIN );	   /* input */
	SB( LID_PORT, LID_PIN );   /* pullup */

	_NOP();			   /* Let things propogate */
	_NOP();

	if (BITISSET( LID_PINS, LID_PIN )) {
		SERIALSTR("L");
		lid = true;
	} else {
		lid = false;
	}
	CB( LIDV_PORT, LIDV_PIN );
	CB( LID_PORT, LID_PIN );

	return lid;
}
