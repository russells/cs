#ifndef ad7303_h_INCLUDED
#define ad7303_h_INCLUDED

#define AD7303_INTEXT 7
#define AD7303_X      6
#define AD7303_LDAC   5
#define AD7303_PDB    4
#define AD7303_PDA    3
#define AD7303_AB     2
#define AD7303_CR1    1
#define AD7303_CR0    0

#endif // ad7303_h_INCLUDED
