#include "config.h"
#include "pins.h"
#include "setbits.h"
#include "truefalse.h"
#include "cpu-speed.h"
#include "toggle-pin.h"
#include "qpn_port.h"
#include <avr/wdt.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/cpufunc.h>	/* _NOP() */


Q_DEFINE_THIS_MODULE(config);


uint8_t configA = 0;
uint8_t configB = 0;

static uint8_t configsread = false;


#define r(name,port,bit,ddr,pins,var)				\
	do {							\
		CB(ddr, bit);		      /* input */	\
		SB(port, bit);		      /* pullup */	\
		_NOP();         /* Wait for pin synchroniser */ \
		_NOP();           /* Needs at least 2 cycles */ \
		if (BS(pins, bit)) {				\
			/* Jumper not connected, pin high */	\
			var = false;				\
		} else {					\
			/* Jumper connected, pin low */		\
			var = true;				\
		}						\
		CB(port, bit);		      /* no pullup */	\
		wdt_reset();					\
		_delay_ms(200);					\
		wdt_reset();					\
		_delay_ms(200);					\
		if (var) {					\
			TOGGLE_ON();				\
		}						\
		wdt_reset();					\
		_delay_ms(100);					\
		TOGGLE_OFF();					\
		wdt_reset();					\
		_delay_ms(100);					\
		TOGGLE_ON();					\
		_delay_ms(100);					\
		wdt_reset();					\
		TOGGLE_OFF();					\
		wdt_reset();					\
	} while (0)


void readconfigs(void)
{
	Q_ASSERT( ! configsread );
	r("A", CONFIGA_PORT, CONFIGA_BIT, CONFIGA_DDR, CONFIGA_PINS, configA);
	r("B", CONFIGB_PORT, CONFIGB_BIT, CONFIGB_DDR, CONFIGB_PINS, configB);
	configsread = true;
}
