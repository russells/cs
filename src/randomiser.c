#include "qpn_port.h"
#include "randomiser.h"
#include "runner.h"
#include "signals.h"
#include "serial.h"
#include "config.h"
#include <stdlib.h>


static QState randomiserInitialState(struct Randomiser *me);
static QState randomiserShortWaitState(struct Randomiser *me);
static QState randomiserLongWaitState(struct Randomiser *me);
static void do_random(void);

struct Randomiser randomiser;


/** Play the last (numerically last) sound after three days (by default). */
#define LIDTIMEOUT ((QTimeEvtCtr)(4 * ((QTimeEvtCtr)(INITIAL_PAUSE_TIME)) * 8640))


void randomiser_ctor(void)
{
	randomiser.counter = 0;
	QActive_ctor(&(randomiser.super), (QStateHandler)randomiserInitialState);
}


static QState randomiserInitialState(struct Randomiser *me)
{
	return Q_TRAN(randomiserShortWaitState);
}


static QState randomiserShortWaitState(struct Randomiser *me)
{
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		QActive_arm(&(me->super), 3);
		return Q_HANDLED();
	case Q_TIMEOUT_SIG:
		me->counter ++;
		do_random();
		if (me->counter < 10) {
			return Q_TRAN(randomiserShortWaitState);
		} else {
			return Q_TRAN(randomiserLongWaitState);
		}
	}
	return Q_SUPER(&QHsm_top);
}


static QState randomiserLongWaitState(struct Randomiser *me)
{
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		QActive_arm(&(me->super), 391);
		return Q_HANDLED();
	case Q_TIMEOUT_SIG:
		do_random();
		return Q_TRAN(randomiserLongWaitState);
	}
	return Q_SUPER(&QHsm_top);
}


Q_ASSERT_COMPILE( sizeof(long int) == 4 );
Q_ASSERT_COMPILE( sizeof(uint32_t) == 4 );

static void do_random(void)
{
	static volatile uint32_t rnum;
	uint8_t *rbytes = (uint8_t *)(&rnum);

#define SWAP(b1,b2)	     \
	do {		     \
		uint8_t tmp; \
		tmp = b1;    \
		b1 = b2;     \
		b2 = tmp;    \
	} while (0)

	rnum = random();
	if ( rbytes[2] > 63) {
		SWAP(rbytes[0], rbytes[1]);
	} else if (rbytes[2] > 128) {
		SWAP(rbytes[1], rbytes[3]);
	} else if (rbytes[2] > 192) {
		SWAP(rbytes[0], rbytes[3]);
	} else {
		SWAP(rbytes[2], rbytes[0]);
	}
	srandom(rnum);
}
