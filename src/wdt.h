#ifndef wdt_h_INCLUDED
#define wdt_h_INCLUDED

void watchdog_start(void);
void watchdog_tick(void);
void watchdog_long(void);
void watchdog_short(void);
void watchdog_stop(void);

#endif // wdt_h_INCLUDED
