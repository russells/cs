#include "qpn_port.h"
#include "lidtimer.h"
#include "runner.h"
#include "signals.h"
#include "serial.h"
#include "config.h"


static QState lidtimerInitialState(struct LidTimer *me);
static QState lidtimerWaitingState(struct LidTimer *me);
static QState lidtimerOnState(struct LidTimer *me);
static QState lidtimerOffState(struct LidTimer *me);


struct LidTimer lidtimer;


/** Play the last (numerically last) sound after three days (by default). */
#define LIDTIMEOUT ((QTimeEvtCtr)(4 * ((QTimeEvtCtr)(INITIAL_PAUSE_TIME)) * 8640))


void lidtimer_ctor(void)
{
	QActive_ctor(&(lidtimer.super), (QStateHandler)lidtimerInitialState);
}


static QState lidtimerInitialState(struct LidTimer *me)
{
	return Q_TRAN(lidtimerWaitingState);
}


static QState lidtimerWaitingState(struct LidTimer *me)
{
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		me->counter = 0;
		return Q_HANDLED();
	case LID_OFF_SIGNAL:
		if (me->counter >= 4) {
			return Q_TRAN(lidtimerOffState);
		} else {
			me->counter ++;
			SERIALSTR("Lidtimer:");
			SERIAL_SEND_INT(me->counter);
			SERIALSTR("\r\n");
			return Q_HANDLED();
		}
	}
	return Q_SUPER(&QHsm_top);
}


static QState lidtimerOffState(struct LidTimer *me)
{
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		SERIALSTR("Lidtimer: off\r\n");
		QActive_disarm(&(me->super));
		return Q_HANDLED();
	case LID_ON_SIGNAL:
		return Q_TRAN(lidtimerOnState);
	}
	return Q_SUPER(&QHsm_top);
}


static QState lidtimerOnState(struct LidTimer *me)
{
	QTimeEvtCtr timeout;
	switch (Q_SIG(me)) {
	case Q_ENTRY_SIG:
		SERIALSTR("Lidtimer: on ");
		if (configB) {
			timeout = 10000;
		} else {
			timeout = LIDTIMEOUT;
		}
		SERIAL_SEND_LONG_INT(timeout);
		SERIALSTR("\r\n");
		QActive_arm(&(me->super), timeout);
		return Q_HANDLED();
	case LID_OFF_SIGNAL:
		return Q_TRAN(lidtimerOffState);
	case Q_TIMEOUT_SIG:
		SERIALSTR("Lidtimer: timeout\r\n");
		SERIAL_DRAIN();
		QActive_post(&(runner.super), LID_ON_LONG_SIGNAL);
		return Q_HANDLED();
	}
	return Q_SUPER(&QHsm_top);
}
