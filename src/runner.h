#ifndef runner_h_INCLUDED
#define runner_h_INCLUDED

#include "qpn_port.h"

struct Runner {
	QActive super;
	/** Time to wait between random sounds. */
	QTimeEvtCtr pauseTime;
	/** Maximum time to wait between random sounds. */
	QTimeEvtCtr maxPauseTime;
	/** Index of the most recent sound played.  We keep this so we don't
	    play the same sound twice in a row. */
	uint8_t lastSound;
};

extern struct Runner runner;

void runner_ctor(void);

#endif // runner_h_INCLUDED
