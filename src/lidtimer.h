#ifndef lidtimer_h_INCLUDED
#define lidtimer_h_INCLUDED

#include "qpn_port.h"

struct LidTimer {
	QActive super;
	uint8_t counter;
};

extern struct LidTimer lidtimer;

void lidtimer_ctor(void);

#endif // lidtimer_h_INCLUDED
