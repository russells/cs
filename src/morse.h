#ifndef morse_h_INCLUDED
#define morse_h_INCLUDED

#include "qpn_port.h"

void morse_assert(char const Q_ROM * const str, int num);

#endif
